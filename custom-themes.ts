
import type { CustomThemeConfig } from '@skeletonlabs/tw-plugin';

export const inno4vac: CustomThemeConfig = {
    name: 'inno4vac',
    properties: {
		// =~= Theme Properties =~=
		"--theme-font-family-base": "system-ui",
		"--theme-font-family-heading": "system-ui",
		"--theme-font-color-base": "0 0 0",
		"--theme-font-color-dark": "255 255 255",
		"--theme-rounded-base": "4px",
		"--theme-rounded-container": "4px",
		"--theme-border-base": "1px",
		// =~= Theme On-X Colors =~=
		"--on-primary": "255 255 255",
		"--on-secondary": "0 0 0",
		"--on-tertiary": "0 0 0",
		"--on-success": "0 0 0",
		"--on-warning": "0 0 0",
		"--on-error": "255 255 255",
		"--on-surface": "255 255 255",
		// =~= Theme Colors  =~=
		// primary | #3285D9 
		"--color-primary-50": "224 237 249", // #e0edf9
		"--color-primary-100": "214 231 247", // #d6e7f7
		"--color-primary-200": "204 225 246", // #cce1f6
		"--color-primary-300": "173 206 240", // #adcef0
		"--color-primary-400": "112 170 228", // #70aae4
		"--color-primary-500": "50 133 217", // #3285D9
		"--color-primary-600": "45 120 195", // #2d78c3
		"--color-primary-700": "38 100 163", // #2664a3
		"--color-primary-800": "30 80 130", // #1e5082
		"--color-primary-900": "25 65 106", // #19416a
		// secondary | #C2ACF1 
		"--color-secondary-50": "246 243 253", // #f6f3fd
		"--color-secondary-100": "243 238 252", // #f3eefc
		"--color-secondary-200": "240 234 252", // #f0eafc
		"--color-secondary-300": "231 222 249", // #e7def9
		"--color-secondary-400": "212 197 245", // #d4c5f5
		"--color-secondary-500": "194 172 241", // #C2ACF1
		"--color-secondary-600": "175 155 217", // #af9bd9
		"--color-secondary-700": "146 129 181", // #9281b5
		"--color-secondary-800": "116 103 145", // #746791
		"--color-secondary-900": "95 84 118", // #5f5476
		// tertiary | #65B625 
		"--color-tertiary-50": "232 244 222", // #e8f4de
		"--color-tertiary-100": "224 240 211", // #e0f0d3
		"--color-tertiary-200": "217 237 201", // #d9edc9
		"--color-tertiary-300": "193 226 168", // #c1e2a8
		"--color-tertiary-400": "147 204 102", // #93cc66
		"--color-tertiary-500": "101 182 37", // #65B625
		"--color-tertiary-600": "91 164 33", // #5ba421
		"--color-tertiary-700": "76 137 28", // #4c891c
		"--color-tertiary-800": "61 109 22", // #3d6d16
		"--color-tertiary-900": "49 89 18", // #315912
		// success | #84cc16 
		"--color-success-50": "237 247 220", // #edf7dc
		"--color-success-100": "230 245 208", // #e6f5d0
		"--color-success-200": "224 242 197", // #e0f2c5
		"--color-success-300": "206 235 162", // #ceeba2
		"--color-success-400": "169 219 92", // #a9db5c
		"--color-success-500": "132 204 22", // #84cc16
		"--color-success-600": "119 184 20", // #77b814
		"--color-success-700": "99 153 17", // #639911
		"--color-success-800": "79 122 13", // #4f7a0d
		"--color-success-900": "65 100 11", // #41640b
		// warning | #ffa200 
		"--color-warning-50": "255 241 217", // #fff1d9
		"--color-warning-100": "255 236 204", // #ffeccc
		"--color-warning-200": "255 232 191", // #ffe8bf
		"--color-warning-300": "255 218 153", // #ffda99
		"--color-warning-400": "255 190 77", // #ffbe4d
		"--color-warning-500": "255 162 0", // #ffa200
		"--color-warning-600": "230 146 0", // #e69200
		"--color-warning-700": "191 122 0", // #bf7a00
		"--color-warning-800": "153 97 0", // #996100
		"--color-warning-900": "125 79 0", // #7d4f00
		// error | #df0c0c 
		"--color-error-50": "250 219 219", // #fadbdb
		"--color-error-100": "249 206 206", // #f9cece
		"--color-error-200": "247 194 194", // #f7c2c2
		"--color-error-300": "242 158 158", // #f29e9e
		"--color-error-400": "233 85 85", // #e95555
		"--color-error-500": "223 12 12", // #df0c0c
		"--color-error-600": "201 11 11", // #c90b0b
		"--color-error-700": "167 9 9", // #a70909
		"--color-error-800": "134 7 7", // #860707
		"--color-error-900": "109 6 6", // #6d0606
		// surface | #495a8f 
		"--color-surface-50": "228 230 238", // #e4e6ee
		"--color-surface-100": "219 222 233", // #dbdee9
		"--color-surface-200": "210 214 227", // #d2d6e3
		"--color-surface-300": "182 189 210", // #b6bdd2
		"--color-surface-400": "128 140 177", // #808cb1
		"--color-surface-500": "73 90 143", // #495a8f
		"--color-surface-600": "66 81 129", // #425181
		"--color-surface-700": "55 68 107", // #37446b
		"--color-surface-800": "44 54 86", // #2c3656
		"--color-surface-900": "36 44 70", // #242c46
		
	}
}