FROM node:18 AS build

WORKDIR /app
COPY . .
RUN npm install
RUN npm run build


FROM node:18 AS deploy-node

WORKDIR /app
RUN rm -rf ./*
COPY --from=build /app/package.json .
COPY --from=build /app/build .
RUN npm install --omit=dev

CMD ["node", "index.js"]