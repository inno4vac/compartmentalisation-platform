export type TannazResponse = {
    grid_info_headers: string[];
    grid_info: string[][];
    volume_headers: string[];
    volume: string[][];
    image: string;
    flow_rate_matrix: string[][];
};

export type ModelParam = {
    key: string;
    value: string;
};