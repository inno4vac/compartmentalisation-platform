export type CadetModelParams = {
    volumes: number[];
    flow_matrix: number[][];
    number_of_components: number;
}

export type CadetModelTracer = {
    compartment_index: number;
    concentration: number[];
    injection_duration: number;
    flow_rate: number;
    start_time: number;
}

export type CadetModelKineticReaction = {
    reactants: number[];
    products: number[];
    k_fwd: number;
    k_bwd: number;
}

export type KineticResponse = {
    image: string;
    time: number[];
    concentration: Record<string, Array<Array<number>>>;
}