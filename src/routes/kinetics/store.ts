import { writable } from 'svelte/store';

export const flow_rate_matrix = writable<Array<Array<number>>>();
export const volumes = writable<number[]>([]);
export const currentstep = writable<number>(0);