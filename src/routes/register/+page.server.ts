import { fail } from '@sveltejs/kit';
export const actions = {
	default: async ({ request, url, locals: { supabase } }) => {
		const formData = await request.formData();
		const email = formData.get('email') as string;
		const password = formData.get('password') as string;
		const confirmPassword = formData.get('confirmpassword') as string;
		const name = formData.get('name') as string;
		if (password !== confirmPassword) {
			return fail(400, { status: 'error', message: 'pw_nomatch' });
		}
		const { error: authError } = await supabase.auth.signUp({
			email,
			password,
			options: { data: { full_name: name }, emailRedirectTo: `${url.origin}/auth/callback` }
		});
		if (authError) {
			console.log('authError', authError.message);
			return fail(409, { status: 'error', message: authError.message });
		}
		return {
			message: 'Successfully registered. Check your email for a link to log in!',
			success: true
		};
	}
};
