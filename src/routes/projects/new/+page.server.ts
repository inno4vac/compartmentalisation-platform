import { fail, redirect } from '@sveltejs/kit';

export const actions = {
	create: async ({ request, locals: { supabase } }) => {
		const formData = await request.formData();
		const name = formData.get('name') as string;
		const description = formData.get('description') as string;
		const isPublic = formData.get('isPublic') === 'on';
		const { error } = await supabase
			.from('projects')
			.insert({
				name: name,
				description: description,
				isPublic: isPublic,
				owner: (await supabase.auth.getUser()).data.user?.id
			})
			.select();
		console.log(error);
		if (error) {
			return fail(401, {
				error: error.message
			});
		}
		return {
			message: 'Successfully logged in!',
			success: true
		};
	}
};
