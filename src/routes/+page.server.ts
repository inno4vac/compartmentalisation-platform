import type { PageServerLoad } from './$types';

export const load: PageServerLoad = async ({ url, locals: { getSession } }) => {
	// if the user is already logged in return them to the account page
	return { url: url.origin };
};
