export const actions = {
	default: async ({ request, url, locals: { supabase } }) => {
		const data = await request.formData();
		const email = data.get('email') as string;
		try {
			const { error } = await supabase.auth.resetPasswordForEmail(email, {
				redirectTo: `${url.origin}/auth/callback?next=/account/update-password`
			});
			if (error) {
				return {
					message: 'invalid_grant',
					status: 'error'
				};
			}
		} catch (error) {
			return {
				message: 'unknown_error',
				status: 'error'
			};
		}
		return {
			message: 'Successfully sent email!',
			status: 'success'
		};
	}
};
