import { goto } from "$app/navigation";
import { toastStore, type ToastSettings } from '@skeletonlabs/skeleton';

export function load(){
    const t: ToastSettings = {
        message: 'Successfully logged out.',
        background: 'bg-success-500',
        timeout : 5000
    };
    setTimeout(() => {
        toastStore.trigger(t);
    }, 1000);
    setTimeout(() => {
	   goto('/login');
	}, 3000);
}