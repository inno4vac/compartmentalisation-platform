import { fail, redirect } from '@sveltejs/kit';

export const actions = {
	login: async ({ request, locals: { supabase }, url }) => {
		const formData = await request.formData();
		const email = formData.get('email') as string;
		const password = formData.get('password') as string;
		const { error } = await supabase.auth.signInWithPassword({ email, password });
		if (error) {
			return fail(401, {
				error: error.message
			});
		}
		
		const redirectTo = url.searchParams.get('redirectTo') ||'/';
		console.log(redirectTo);
		if(redirectTo){
			throw redirect(303, redirectTo);
		}
		return {
			message: 'Successfully logged in!',
			success: true
		};
	}
};
