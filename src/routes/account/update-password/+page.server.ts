import { fail } from '@sveltejs/kit';

export const actions = {
	default: async ({ request, url, locals: { supabase } }) => {
		const formData = await request.formData();
		const password = formData.get('password') as string;
		const confirmPassword = formData.get('confirmpassword') as string;
		if (password !== confirmPassword) {
			return fail(400, { error: 'Passwords do not match' });
		}
		await supabase.auth.updateUser({ password });
		return {
			message: 'Successfully updated password.',
			success: true
		};
	}
};
