import xlsx from 'xlsx';

export const readExcelFile = async (file: File) => {
    try {
        const data = await file.arrayBuffer();
        const workbook = xlsx.read(data);
        const sheetName = workbook.SheetNames[0];
        const sheet = workbook.Sheets[sheetName];
        const temp = xlsx.utils.sheet_to_json(sheet);
        const result: object[] = []
        temp.forEach((res) => {
            res ? result.push(res) : null;
        });

        return result;
    } catch (error) {
        console.log(error);
    }
}

export const readCSVFileTo2DArray = async (file: File) => {
    try {
        const data = await file.text();
        const result = data.split('\n').map((row) => {
            return row.split(',');
        });
        //remove last row
        result.pop();
        return result;
    } catch (error) {
        console.log(error);
    }
}