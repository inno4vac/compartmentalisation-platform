// src/hooks.server.ts
import { env } from '$env/dynamic/public';
import { createSupabaseServerClient } from '@supabase/auth-helpers-sveltekit';
import type { Handle } from '@sveltejs/kit';
import { redirect, error } from '@sveltejs/kit';


export const handle: Handle = async ({ event, resolve }) => {
	event.locals.supabase = createSupabaseServerClient({
		supabaseUrl: env.PUBLIC_SUPABASE_URL,
		supabaseKey: env.PUBLIC_SUPABASE_ANON_KEY,
		event
	});

	/**
	 * a little helper that is written for convenience so that instead
	 * of calling `const { data: { session } } = await supabase.auth.getSession()`
	 * you just call this `await getSession()`
	 */
	event.locals.getSession = async () => {
		const {
			data: { session }
		} = await event.locals.supabase.auth.getSession();
		return session;
	};
	if (
		!event.url.pathname.startsWith('/login') &&
		!event.url.pathname.startsWith('/register') &&
		!event.url.pathname.startsWith('/forgot_password') &&
		!event.url.pathname.startsWith('/auth')
	) {
		const session = await event.locals.getSession();
		//get last part of url path
		if ((!session && event.url.pathname === '/') || event.url.pathname.startsWith('/logout')) {
			throw redirect(303, '/login');
		} else if (!session) {
			throw redirect(303, '/login?redirectTo=' + event.url.pathname.split('/').pop());
		}
	}

	return resolve(event, {
		filterSerializedResponseHeaders(name) {
			return name === 'content-range';
		}
	});
};
