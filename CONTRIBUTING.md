# Contributer Guide

## Development Workflow

1. If it's your first time contributing:
   - Go to the repo <https://gitlab.gbar.dtu.dk/inno4vac/compartmentalisation-platform>
   - Click the "fork" button in order to create your own copy of the repository
   - Clone the project to copy it to your local machine
     `git clone https://gitlab.gbar.dtu.dk/inno4vac/compartmentalisation-platform.git`
   - Navigate to the folder compartmentalisaton-api and add this repository as the upstream repository:
     `git remote add upstream https://gitlab.gbar.dtu.dk/inno4vac/compartmentalisation-platform.git`
   - Now you will have two remote repositories:
     - `upstream`, which refers to the `compartmentalisation-platform` repository
     - `origin`, which is your own personal fork
   - Next, setup your build environment:
        - Clone the repository 
            ```
            git clone https://gitlab.gbar.dtu.dk/inno4vac/compartmentalisation-platform.git
            ```
        - Navigate to the project directory
            ```
            cd compartmentalisation-platform`
            ```
        - Install the dependencies:
            ```
            npm install
            ```

        - Start development instance
            ```
            npm run dev
            ```
        
2. Develop your contribution:
   - Pull the latest changes from the upstream repository:
     `git checkout main`
     `git pull upstream main`
   - Create a branch for the feature you will work on. **Make sure name the branch something sensible since it will appear in the commit message**
     `git checkout -b bugfix-for-issue-1000 main`
   - Commit locally as you progress on the feature (using `git add` and `git commit`)
3. Test your contribution:
   - Run the test suite locally. Running the tests locally helps you find mistakes in your feature, and also eases the pressure on the continuous integration (CI) pipeline.
4. Ensure your contribution is properly formatted and linted.
5. Submit your contribution:
   - Push your changes back to your fork on GitLab:
     `git push origin bugfix-for-issue-1000`
   - Go to GitLab. The new branch will show up with a green Pull Request Button. Make sure the Pull Request describes the feature or the issue number that was fixed. Click the green button.
6. Wait for your change to be reviewed:
   - Every Pull Request update triggers a set of CI services that checks the code is up to standard and passes the tests. If it doesn't live up to the the standards, the Pull Request will be declined. Inspect the log to find out why
   - Reviewers and other developers will write comments on your Pull Request to help you improve the implementation, documentation and style.
   - To update your Pull Request, make your changes and commit and push your changes. This will trigger the CI services again


## Diverging from `upstream main`

- If GitLab can't merge the branch of your Pull Request automatically, merge the `upstream main` branch into yours:
  `git fetch upstream main > git merge upstream/main`
- If any conflicts occur, review them before continuing.
- After reviewing the conflicts and solving them, you can run `git add` and `git commit`

## Guidelines

- All comitted code should have tests
- All code should be documented

## Documentation

If you are contributing to a new algorithm or improving an existing one, please provide a resource in the docstring. Use the [Chicago Citation Style](https://en.wikipedia.org/wiki/The_Chicago_Manual_of_Style) for published papers and add DOI links if possible. If there is a paywall add a link to the arXiv version or any public copy of the paper.

```
Dickstein, Morris. “A Literature of One’s Own: The Question of Jewish Book Awards.” Princeton University Library Chronicle 63, no. 1–2 (Winter 2002): 70–74. https://doi.org/10.25290/prinunivlibrchro.63.1-2.0070.
```

If the docstrings contain math symbols, make sure to use raw strings (`r"""`) to ensure backslashes are handled correctly.
An example math formula:

`Ax = \lamba x`

And inline:

`$\frac{a+b}{2}`

## Bugs0

Report bugs on our GitLab, under the [issues page](https://gitlab.gbar.dtu.dk/inno4vac/compartmentalisation-platform/-/issues)
