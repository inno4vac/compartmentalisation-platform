# Compartmentalisation Platform

* Available at [https://platform-image-pfwhpn5qeq-ew.a.run.app](https://platform-image-pfwhpn5qeq-ew.a.run.app)

## Description

This project is the production revison of the compartmentalisation platform designed to help researchers share their work within compartmentalisation and bio-reactor simulation.

## Installation

To install the project, follow these steps:

1. Clone the repository: `git clone https://gitlab.gbar.dtu.dk/inno4vac/compartmentalisation-platform.git`
2. Navigate to the project directory: `cd compartmentalisation-platform`
3. Install the dependencies: `npm install`

## Usage

Make sure you have setup environment file specifiying PUBLIC_SUPABASE_URL, PUBLIC_SUPABASE_ANON_KEY, and PUBLIC_API_URL.

1. Start development instance `npm run dev`

Alternatively start using Docker `docker-compose up -d --build`

## Adding new models
When a new model has been implemented in the [API](https://gitlab.gbar.dtu.dk/inno4vac/compartmentalisation-api) project, the platform should be updated accordingly.

The `ModelDropdown.svelte` component should be updated to contain the new model as an option.
```
<script>
	export let selectedModel = 'cadet';
	export let isDisabled = false;
</script>

<select class="select" id="modelSelect" bind:value={selectedModel} disabled={isDisabled}>
	<option value="cadet">CADET-Process</option>
    <option value="newmodel">Model Name</option>
</select>
```
Then the `ModelParameterSelector.svelte` component should be updated to fetch parameters from the new endpoint if the the new model is selected.
```
async function fetchParamsForModel(model: string) {
        switch (model) {
            case 'cadet':
                ...
            case 'newmodel':
                //fetch here
            default:
                break;
        }
    }
```
Finally update the process function to send the data to the new endpoint.